/*
 *  IMT3801 Multi-threaded Programming - Task 5.1
 *
 *   121240 - 12HBSPA
 *   Tellef Møllerup Åmdal
 */

#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <map>

#include "Callbacks.h"

bool quit = false;

std::mutex mCalculator;
std::mutex mPrinter;

std::queue<std::string> toCalculator;
std::queue<std::pair<std::string, int>> toPrinter;

typedef std::string (*charConvert)(char);

std::map<char, charConvert> charToFunction;

std::string QuitApplication(char key);
std::string AllTheCallbacks(char key);


/**
 * @fn  void ValueReader();
 *
 * @brief   Value reader thread.
 */

void ValueReader();

/**
 * @fn  void ValueCalculator();
 *
 * @brief   Value calculating thread.
 */

void ValueCalculator();

/**
 * @fn  void ValuePrinter();
 *
 * @brief   Value printer thread.
 */

void ValuePrinter();

/**
 * @fn  int main()
 *
 * @brief   Main entry-point for this application.
 *
 * @return  Exit-code for the process - 0 for success, else an error code.
 */

int main() {

    charToFunction['q'] = &QuitApplication;
    charToFunction[' '] = &AllTheCallbacks;

    for(int i = 0; i < 5; i++) {
        charToFunction['1' + i] = &Callbacks::getKey;
    }

    std::thread input(ValueReader);
    std::thread calculator(ValueCalculator);
    std::thread printer(ValuePrinter);

    input.join();
    calculator.join();
    printer.join();

    return 0;
}


void ValueReader() {
    char input;
    std::string value;

    while (!quit) {
        std::this_thread::sleep_for(std::chrono::microseconds(25));

        std::unique_lock<std::mutex> lock(mPrinter);
        if(toPrinter.size() != 0) {
            continue;
        }
        std::cin.get(input);

        try {
            value = charToFunction.at(input)(input);

            std::lock_guard<std::mutex> guard(mCalculator);
            toCalculator.push(value);
        } catch (std::out_of_range e){
            continue;
        }
    }

    printf("ValueReader() quiting\n");
}

void ValueCalculator() {

    while (!quit) {
        std::this_thread::sleep_for(std::chrono::microseconds(25));

        std::unique_lock<std::mutex> inputLock(mCalculator);
        if (toCalculator.size() == 0) {
            continue;
        }

        auto value = toCalculator.front();
        toCalculator.pop();
        inputLock.unlock();

        int result = value.size();

        if(result == 0) {
            continue;
        }

        std::unique_lock<std::mutex> outputLock(mPrinter);
        toPrinter.push({value, result});
    }

    printf("ValueCalculator() quiting\n");
}

void ValuePrinter() {

    while (!quit) {
        std::this_thread::sleep_for(std::chrono::microseconds(25));

        std::unique_lock<std::mutex> lock(mPrinter);
        if (toPrinter.size() == 0) {
            continue;
        }

        auto value = toPrinter.front();
        toPrinter.pop();
        lock.unlock();

        printf("Value: %s with length: %d\n", value.first.c_str(), value.second);
    }

    printf("ValuePrinter() quiting\n");
}

std::string QuitApplication(char key) {
    quit = true;

    return "";
}

std::string AllTheCallbacks(char key) {

    for(char c = '1'; c <= '5'; c++) {
        charToFunction.at(c)(c);
    }

    return "";
}