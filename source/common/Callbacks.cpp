#include "Callbacks.h"

#include <map>

typedef std::string (*callbackSign)();


std::map<char, callbackSign> callBackMapping = std::map<char, callbackSign> ({
        {'1', &Callbacks::getKey1},
        {'2', &Callbacks::getKey2},
        {'3', &Callbacks::getKey3},
        {'4', &Callbacks::getKey4},
        {'5', &Callbacks::getKey5}
});

std::map<char, callbackSign> callBackMappingSwap = std::map<char, callbackSign> ({
        {'1', &Callbacks::getKey5},
        {'2', &Callbacks::getKey4},
        {'3', &Callbacks::getKey1},
        {'4', &Callbacks::getKey2},
        {'5', &Callbacks::getKey3}
});

std::string Callbacks::getKey(char key) {
    callbackSign returnValue = callBackMapping.at(key);

    callBackMapping[key] = callBackMappingSwap.at(key);
    callBackMappingSwap[key] = returnValue;

    return returnValue();
}

std::string Callbacks::getKey1() {
    return "One";
}

std::string Callbacks::getKey2() {
    return "Two";
}

std::string Callbacks::getKey3() {
    return "Three";
}

std::string Callbacks::getKey4() {
    return "Four";
}

std::string Callbacks::getKey5() {
    return "Five";
}