/*
 *  IMT3801 Multi-threaded Programming - Task 5.2
 *
 *   121240 - 12HBSPA
 *   Tellef Møllerup Åmdal
 */

#include <stdlib.h>
#include <pthread.h>

#include <iostream>
#include <queue>
#include <map>

#include "Callbacks.h"

bool quit = false;

pthread_mutex_t mCalculator;
pthread_mutex_t mPrinter;

pthread_cond_t condCalculator;
pthread_cond_t condPrinter;

std::queue<std::string> toCalculator;
std::queue<std::pair<std::string, int>> toPrinter;

typedef std::string (*charConvert)(char);

std::map<char, charConvert> charToFunction;

std::string QuitApplication(char key);
std::string AllTheCallbacks(char key);

const timespec defaultInterval = {0, 1};

/**
* @fn  void ValueReader();
*
* @brief   Value reader thread.
*/

void* ValueReader(void *);

/**
* @fn  void ValueCalculator();
*
* @brief   Value calculating thread.
*/

void* ValueCalculator(void *);

/**
* @fn  void ValuePrinter();
*
* @brief   Value printer thread.
*/

void* ValuePrinter(void *);

/**
* @fn  int main()
*
* @brief   Main entry-point for this application.
*
* @return  Exit-code for the process - 0 for success, else an error code.
*/

int main() {

    pthread_mutex_init(&mCalculator, NULL);
    pthread_mutex_init(&mPrinter, NULL);


    charToFunction['q'] = &QuitApplication;
    charToFunction[' '] = &AllTheCallbacks;

    for(int i = 0; i < 5; i++) {
        charToFunction['1' + i] = &Callbacks::getKey;
    }

    pthread_t input;
    pthread_t calculator;
    pthread_t printer;

    pthread_create(&input, NULL, &ValueReader, NULL);
    pthread_create(&calculator, NULL, &ValueCalculator, NULL);
    pthread_create(&printer, NULL, &ValuePrinter, NULL);

    pthread_join(input, NULL);
    pthread_join(calculator, NULL);
    pthread_join(printer, NULL);

    pthread_mutex_destroy(&mCalculator);
    pthread_mutex_destroy(&mPrinter);

    return 0;
}


void* ValueReader(void *) {
    char input;
    std::string value;

    while (!quit) {
        nanosleep(&defaultInterval, NULL);

        pthread_mutex_lock(&mPrinter);
        int size = toPrinter.size();
        pthread_mutex_unlock(&mPrinter);

        if(size != 0) {
            continue;
        }

        std::cin.get(input);

        try {
            value = charToFunction.at(input)(input);

        } catch (std::out_of_range e){
            continue;
        }

        pthread_mutex_lock(&mPrinter);
        toCalculator.push(value);
        pthread_mutex_unlock(&mPrinter);
    }

    printf("ValueReader() quiting\n");
}

void* ValueCalculator(void *) {

    while (!quit) {
        nanosleep(&defaultInterval, NULL);

        pthread_mutex_lock(&mCalculator);
        if (toCalculator.size() == 0) {
            pthread_mutex_unlock(&mCalculator);
            continue;
        }

        auto value = toCalculator.front();
        toCalculator.pop();
        pthread_mutex_unlock(&mCalculator);

        int result = value.size();

        if(result == 0) {
            continue;
        }

        pthread_mutex_lock(&mPrinter);
        toPrinter.push({value, result});
        pthread_mutex_unlock(&mPrinter);
    }

    printf("ValueCalculator() quiting\n");
}

void* ValuePrinter(void *) {

    while (!quit) {
        nanosleep(&defaultInterval, NULL);

        pthread_mutex_lock(&mPrinter);
        if (toPrinter.size() == 0) {
            pthread_mutex_unlock(&mPrinter);
            continue;
        }

        auto value = toPrinter.front();
        toPrinter.pop();
        pthread_mutex_unlock(&mPrinter);

        printf("Value: %s with length: %d\n", value.first.c_str(), value.second);
    }

    printf("ValuePrinter() quiting\n");
}

std::string QuitApplication(char key) {
    quit = true;

    return "";
}

std::string AllTheCallbacks(char key) {

    for(char c = '1'; c <= '5'; c++) {
        charToFunction.at(c)(c);
    }

    return "";
}